
#!/bin/bash

git clone https://gitlab.indiscale.com/caosdb/src/caosdb-cpplib.git
cd caosdb-cpplib
# check if there is a crooesponding cpp branch. Use the default branch
# if there isn't.
if (git show-ref --verify --quiet refs/heads/$F_BRANCH) ; then
    git checkout $F_BRANCH
else
    git checkout $CPP_DEFAULT_BRANCH
fi
git submodule update --init --recursive
mkdir build
cd build
conan install .. -s "compiler.libcxx=libstdc++11"
cmake ..
cmake --build .
cmake --install .
